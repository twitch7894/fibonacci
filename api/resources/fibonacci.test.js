const Fibonacci = require("./fibonacci.controller");

test('add 2 return 3', () => {
    expect(Fibonacci(3)).toBe(2);
});

test('add 6 return 8', () => {
    expect(Fibonacci(6)).toBe(8);
});