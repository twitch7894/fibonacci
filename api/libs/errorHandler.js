exports.errorsProduction = (err, req, res, next) => {
  res.status(err.status || 500)
  res.send({
    message: err.message
  })
};

exports.errorsDeveloper = (err, req, res, next) => {
  res.status(err.status || 500)
  res.send({
    message: err.message,
    stack: err.stack || ''
  })
};
