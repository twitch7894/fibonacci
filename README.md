# FIBONACCI


## Starting 🚀


See Deployment to learn how to deploy the project.


### Pre requirements 📋

requirement to have docker installed <a>
    <img width="40"  src="https://linuxcenter.es/images/icagenda/docker.png">
  </a>

```
[install docker](https://docs.docker.com/install/)
```

### Installation 🔧

What you must execute to have a development environment running


```
docker-compose up
```


## Routes ⚙️

Route Structure

### Fibonacci 🔩

POST 

```
http://localhost:3000/fibonacci
```


## BODY JSON 📦
```
{
    "n": 3
}
```
