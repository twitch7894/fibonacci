const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const logger = require("./utils/logger");
const config = require("./config");
const errorHandler = require("./api/libs/errorHandler");
const FibonacciRoutes = require("./api/resources/fibonacci.routes");

const app = express();
app.use(bodyParser.json());
app.use(cors())
app.use(morgan("short", {
  stream: {
    write: message => logger.info(message.trim())
  }
}));

app.use("/fibonacci", FibonacciRoutes);

if (config.Environment === "prod") {
  app.use(errorHandler.errorsProduction);
} else {
  app.use(errorHandler.errorsDeveloper);
}

const server = app.listen(config.port, () => {
  logger.info(`Listening in the port ${config.port}.`);
});

module.exports = {
  app, server
};
