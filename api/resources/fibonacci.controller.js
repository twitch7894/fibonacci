const FibonacciController  = (n) => {
    let f1 = 0;
	let f2 = 1;
	let f;

	if(n <= 1){
		return n;
	}
	for(let i = 2; i <= n; i++){
		f = f1 + f2;
		f1 = f2;
		f2 = f;
	}
	
	return f;

};

module.exports = FibonacciController;

