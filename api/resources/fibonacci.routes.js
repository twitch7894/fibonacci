const express = require("express");

const FibonacciRoutes = express.Router();
const validateFibonacci = require("./fibonacci.validate");
const log = require("../../utils/logger");
const FibonacciController = require("./fibonacci.controller");

FibonacciRoutes.post("/", validateFibonacci, (req, res) => {
       const { n }= req.body;
       log.info(`the value ${n} and result ${FibonacciController(n)} are correct`);
       res.status(200).json(FibonacciController(n));
});

module.exports = FibonacciRoutes;
