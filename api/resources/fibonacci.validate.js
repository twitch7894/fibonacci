const Joi = require("joi");
const log = require("../../utils/logger");

const blueprintFibonacci = Joi.object().keys({
    n: Joi.number().required()
});

let validateFibonacci = (req, res, next) => {
    const result = Joi.validate(req.body, blueprintFibonacci)
    const { error } = result;
    const valid = error == null;
    if (valid) {
        next()
    } else {
        const { details } = error;
        const message = details.map(i => i.message).join(',');
        log.warn("the next process is not valid", req.body, JSON.stringify(message))
        res.status(400).json({ error: message })
    }
  };
  

module.exports = validateFibonacci;

